﻿using System;
using System.IO;
namespace M09UF2E1__RoigCarlos
{
    static class ManipulateArchive
    {
        public static int CountLinesText(string fitxer)
        {
            int lines = 0;
            try
            {
                using (var sr = File.OpenText(PathWithDir("../../MyFiles", fitxer)))
                {
                    string s;
                    while ((s = sr.ReadLine()) != null)
                        lines += 1;
                }
                return lines;
            }
            catch (Exception)
            {
                Console.WriteLine("No tens permisos de lectura o no existeix");
                return -1;
            }
        }
        public static string PathWithDir(string path1, string dir)
        {
            path1 += "/" + dir;
            return path1;
        }
    }
}
