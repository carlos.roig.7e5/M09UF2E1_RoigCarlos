﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace M09UF2E1__RoigCarlos
{
     class Prova
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            
            var prova = new Prova();
            prova.Inici();
        }

        void Inici()
        {
            Console.WriteLine(Environment.CurrentDirectory);
            do
            {
                MostratMenu();
            } while (!OpcionsMenu());
        }

        void MostratMenu()
        {
            InOutData.ShowString("[1]. Mostrar les línies d'un fitxer.");
            InOutData.ShowString("[2]. Thread que mostra que mostri i compti quantes línies té el fitxer.");
            InOutData.ShowString("[3]. 3 threads que compte les línies de diferents fitxers.");
            InOutData.ShowString("[0]. Sortir.");
        }

        bool OpcionsMenu()
        {
            switch (InOutData.InString("Introdueix una opció del 0 al 1."))
            {
                case "0":
                    return true;
                case "1":
                    FuncioQueCompteLesLiniesDelText();
                    break;
                case "2":
                    ThreadCountLinesAndShowLetter("PocoOriginal.txt");
                    break;
                case "3":
                    ThreeTheards();
                    break;
            }
            return false;
        }
        void FuncioQueCompteLesLiniesDelText()
        {
           InOutData.ShowString(ManipulateArchive.CountLinesText("PocoOriginal.txt").ToString());
        }

        void ThreadCountLinesAndShowLetter(string fitxer)
        {
            Thread countLines = new Thread(Threads.ReadAndCountLines);
            countLines.Start(fitxer);
        }

        void ThreeTheards()
        {
            var fitxers = new string[] { "PocoOriginal.txt", "Hola.txt", "Adios.txt"};
            foreach (var fitxer in fitxers)
            {
                ThreadCountLinesAndShowLetter(fitxer);
            }
        }
    }
}
